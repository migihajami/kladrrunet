namespace Kladrrunet
{
    public interface IKladrEntity<T>
    {
         T FromObjectArray(object[] row);
    }
}