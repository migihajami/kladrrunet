using System;
using System.IO;
using System.Text;
using DotNetDBF;

namespace Kladrrunet
{
    public class KladrDbfBase: IDisposable
    {
        private const int Codepage = 866;
        protected DBFReader reader;
        private Stream _fileStream;
        private string _filename;

        public KladrDbfBase(String filename)
        {
            _filename = filename;
            _fileStream = File.Open(filename, FileMode.Open, FileAccess.Read);
            _createDbfReader();
        }

        /// <summary>
        /// Creates DbfReader on _fileStream and set CP866 encoding for it.
        /// </summary>
        private void _createDbfReader()
        {
            reader = new DBFReader(_fileStream);
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            reader.CharEncoding = Encoding.GetEncoding(Codepage);
        }

        /// <summary>
        /// Get next record from DBF
        /// </summary>
        /// <returns></returns>
        protected object[] NextRecord(){
            return reader.NextRecord();
        }

        public void Dispose()
        {
            //reader.Dispose();
            //_fileStream.Dispose();
        }

        public void ResetReader()
        {
            reader.Dispose();
            _fileStream = File.Open(_filename, FileMode.Open, FileAccess.Read);
            _createDbfReader();
        }
    }
}