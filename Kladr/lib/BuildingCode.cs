using System;

namespace Kladrrunet
{
    public class BuildingCode : StreetCode
    {
        private const int CODE_LENGTH = 19;
        private const int DOM_INDEX = 15;
        private const int DOM_LEN = 4;

        public string Building { get; set; }
        public BuildingCode(string code) : base(code)
        {
            if (code.Length < CODE_LENGTH)
            {
                throw new Exception($"Code length must be {CODE_LENGTH}");
            }
            Building = code.Substring(DOM_INDEX, DOM_LEN);
        }
    }
}