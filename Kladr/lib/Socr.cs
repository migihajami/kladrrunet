using System;
using Kladrrunet;

namespace Kladrrunet
{
    /// <summary>
    /// LEVEL, SCNAME, SOCRNAME, KOD_T_ST
    /// </summary>
    public class Socr: IKladrEntity<Socr>
    {
        private const int LEVEL_INDEX = 0, 
            SCNAME_INDEX = 1,
            SORNAME_INDEX = 2,
            CODE_INDEX = 3;

        public int Level{ get; set; }
        public string Scname { get; set; }
        public string SocrName { get; set; }
        public string Code { get; set; }

        public Socr FromObjectArray(object[] row)
        {
            Level = Convert.ToInt32(row[LEVEL_INDEX]);
            Scname = row[SCNAME_INDEX].ToString();
            SocrName = row[SORNAME_INDEX].ToString();
            Code = row[CODE_INDEX].ToString();
            return this;
        }
    }
}