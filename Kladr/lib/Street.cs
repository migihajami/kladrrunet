using System;

namespace Kladrrunet
{
    /// <summary>
    /// NAME, SOCR, CODE, INDEX, GNINMB, UNO, OCATD
    /// </summary>
    public class Street: IKladrEntity<Street>
    {
        protected const int NAME_INDEX = 0,
            SOCR_INDEX = 1,
            CODE_INDEX = 2,
            POSTINDEX_INDEX = 3,
            GNINMB_INDEX = 4,
            UNO_INDEX = 5,
            OCATD_INDEX = 6;

        public string Name { get; set; }
        public string Socr { get; set; }
        public StreetCode Code { get; set; }
        public string PostIndex { get; set; }
        public string Gninmb { get; set; }
        public string Uno { get; set; }
        public string Ocatd { get; set; }

        public Street FromObjectArray(object[] row)
        {
            Name = row[NAME_INDEX].ToString();
            Socr = row[SOCR_INDEX].ToString();
            Code = new StreetCode(row[CODE_INDEX].ToString());
            PostIndex = row[POSTINDEX_INDEX].ToString();
            Gninmb = row[GNINMB_INDEX].ToString();
            Uno = row[UNO_INDEX].ToString();
            Ocatd = row[OCATD_INDEX].ToString();
            return this;
        }
    }
}