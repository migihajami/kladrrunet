using System;

namespace Kladrrunet
{
    public class KladrCode
    {
        //СС РРР ГГГ ППП АА
        //01 234 567 890 12
        public string Subject { get; set; }
        public string Rajon { get; set; }
        public string City { get; set; }
        public string Locality { get; set; }
        public string Actuality { get; set; }
        public string FullCode { 
            get 
            {
                return $"{Subject}{Rajon}{City}{Locality}{Actuality}";
            }
        }
        
        #region [ Length and indexes ]
        protected const int SUBJECT_INDEX = 0;
        protected const int SUBJECT_LEN = 2;
        protected const int RAJON_INDEX = 2;
        protected const int RAJON_LEN = 3;
        protected const int CITY_INDEX = 5;
        protected const int CITY_LEN = 3;
        protected const int LOCALITY_INDEX = 8;
        protected const int LOCALITY_LEN = 3;
        protected const int ACTUALITY_INDEX = 11;
        protected const int ACTUALITY_LEN = 2;
        private const int CODE_LENGTH = 13;
        #endregion

        public KladrCode(string code)
        {
            if (string.IsNullOrEmpty(code) || code.Length < CODE_LENGTH)
            {
                throw new Exception("Code length must be 13");
            }

            Subject = code.Substring(SUBJECT_INDEX, SUBJECT_LEN);
            Rajon = code.Substring(RAJON_INDEX, RAJON_LEN);
            City = code.Substring(CITY_INDEX, CITY_LEN);
            Locality = code.Substring(LOCALITY_INDEX, LOCALITY_LEN);
            Actuality = code.Substring(ACTUALITY_INDEX, ACTUALITY_LEN);
        }
    }
}