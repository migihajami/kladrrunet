using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Kladrrunet
{
    public class KladrReader<T>:KladrDbfBase, IEnumerable<T> where T: IKladrEntity<T>, new()
    {
        public KladrReader(String filename): base(filename)
        {

        }

        public IEnumerator<T> GetEnumerator()
        {
            object[] _currentRow;
           while ((_currentRow = this.NextRecord()) != null)
            {
                yield return new T().FromObjectArray(_currentRow);
            }
            this.Reset();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            object[] _currentRow;
            while ((_currentRow = this.NextRecord()) != null)
            {
                yield return new T().FromObjectArray(_currentRow);
            }
            this.Reset();
        }

        public void Reset()
        {
            this.ResetReader();
        }
    }
}