using System;

namespace Kladrrunet
{
    public class StreetCode : KladrCode
    {
        private const int CODE_LENGTH = 15;
        protected const int STREET_INDEX = 13;
        protected const int STREET_LEN = 4;

        public string Street { get; set; }

        public StreetCode(string code) : base(code)
        {
            if (code.Length < CODE_LENGTH)
            {
                throw new Exception($"Code length must be {CODE_LENGTH}");
            }
            Street = code.Substring(STREET_INDEX, STREET_LEN);
        }
    }
}