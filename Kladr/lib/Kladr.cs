using System;

namespace Kladrrunet
{
    /// <summary>
    /// Fields: NAME,	SOCR,	CODE,	INDEX,	GNINMB,	UNO,	OCATD,	STATUS
    /// </summary>
    public class Kladr: Kladrrunet.IKladrEntity<Kladr>
    {
        protected const int NAME_INDEX = 0, 
                            SOCR_INDEX = 1, 
                            CODE_INDEX = 2, 
                            POSTINDEX_INDEX = 3, 
                            GNINMB_INDEX = 4, 
                            UNO_INDEX = 5, 
                            OCATD_INDEX = 6, 
                            STATUS_INDEX = 7;

        public string Name { get; set; }
        public string Socr { get; set; }
        public KladrCode Code { get; set; }
        public string PostIndex { get; set; }
        public string Gninmb { get; set; }
        public string Uno { get; set; }
        public int Status { get; set; }

        public Kladr FromObjectArray(object[] row)
        {
            Name = row[NAME_INDEX].ToString();
            Socr = row[SOCR_INDEX].ToString();
            Code = new KladrCode(row[CODE_INDEX].ToString());
            PostIndex = row[POSTINDEX_INDEX].ToString();
            Gninmb = row[GNINMB_INDEX].ToString();
            Uno = row[UNO_INDEX].ToString();
            Status = Convert.ToInt32(row[STATUS_INDEX]);
            return this;
        }
    }
}