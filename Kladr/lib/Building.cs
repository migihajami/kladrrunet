using System;

namespace Kladrrunet
{
    /// <summary>
    /// NAME, KORP, SOCR, CODE, INDEX, GNINMB, UNO, OCATD
    /// </summary>
    public class Building: IKladrEntity<Building>
    {
        protected const int NAME_INDEX = 0,
            KORP_INDEX = 1,
            SOCR_INDEX = 2,
            CODE_INDEX = 3,
            POSTINDEX_INDEX = 4,
            GNINMB_INDEX = 5,
            UNO_INDEX = 6,
            OCATD_INDEX = 7;

        public string Name { get; set; }
        public string Korp { get; set; }
        public string Socr { get; set; }
        public BuildingCode Code { get ;set; }
        public string PostIndex { get; set; }
        public string Gninmb { get ;set ;}
        public string Uno { get; set; }
        public string Ocatd { get; set; }

        public Building FromObjectArray(object[] row)
        {
            Name = row[NAME_INDEX].ToString();
            Korp = row[KORP_INDEX].ToString();
            Socr = row[SOCR_INDEX].ToString();
            Code = new BuildingCode(row[CODE_INDEX].ToString());
            PostIndex = row[POSTINDEX_INDEX].ToString();
            Gninmb = row[GNINMB_INDEX].ToString();
            Uno = row[UNO_INDEX].ToString();
            Ocatd = row[OCATD_INDEX].ToString();
            return this;
        }
    }
}