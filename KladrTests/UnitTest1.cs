using System;
using Kladrrunet;
using Xunit;

namespace KladrTests
{
    public class UnitTest1
    {
        [Fact]
        public void TestKladrEnumerator()
        {
            var reader = new KladrReader<Kladr>("/Users/algol/Desktop/kladr/data/KLADR.DBF");
            int i =0;
            foreach (var r in reader)
            {
                i++;
            }
            Assert.True(i > 0);

            //Check reset
            i = 0;
            foreach (var r in reader)
            {
                i++;
            }
            Assert.True(i > 0);
        }
    }
}
